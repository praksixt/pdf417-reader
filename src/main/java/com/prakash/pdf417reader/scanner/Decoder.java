package com.prakash.pdf417reader.scanner;

import java.util.HashMap;

/**
 *
 * @author Prakash Palnati
 */
public class Decoder {
    static final char PDF_LINEFEED = '\n';
    static final char PDF_RECORDSEP = (char) 30;
    static final char PDF_SEGTERM = '\n';
    static final String PDF_FILETYPE = "ANSI ";
    static final HashMap<String, String> fields;
    static {
        fields = new HashMap<String, String>();

        fields.put("DAA", "Name");
        fields.put("DLDAA", "Name");
        fields.put("DAB", "LastName");
        fields.put("DCS", "LastName");
        fields.put("DAC", "FirstName");
        fields.put("DCT", "FirstName");
        fields.put("DAD", "MiddleName");

        fields.put("DBC", "Sex");
        fields.put("DAU", "Height");
        fields.put("DAY", "EyeColor");

        fields.put("DAG", "Address");
        fields.put("DAI", "City");
        fields.put("DAN", "City");
        fields.put("DAJ", "State");
        fields.put("DAO", "State");
        fields.put("DAK", "ZipCode");
        fields.put("DAP", "Zipcode");
        fields.put("DCG", "Country");

        fields.put("DBB", "DOB");
        fields.put("DAQ", "DriverLicenseNumber");
        fields.put("DBD", "LicenseIssuedDate");
        fields.put("DBA", "LicenseExpirationDate");
    }

    protected HashMap headers;
    protected HashMap<String, String> subfile;

    public Decoder(String data) {
        subfile = decodeSubFile(data);
    }



    public HashMap<String, String> getSubFile() {
        return subfile;
    }

    protected HashMap<String, String> decodeSubFile(String data) {

        String[] lines = data.split("\n");
        HashMap<String, String> hm = new HashMap<String, String>();
        for (String l : lines) {
            if (l.length() > 3) {
                String key = l.substring(0, 3);
                String value = l.substring(3);
                if (fields.get(key) != null) {
                    hm.put(fields.get(key), value);
                }
            }
        }
        return hm;
    }

}