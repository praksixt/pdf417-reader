package com.prakash.pdf417reader.scanner;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

/**
 *
 * @author Prakash Palnati
 */
public class QRCodeReader {

    public static String decodeQRCode(InputStream qrCodeimage) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        Map<DecodeHintType, Object> hints = new EnumMap<>(DecodeHintType.class);

        hints.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.of(BarcodeFormat.PDF_417));
        //hints.put(DecodeHintType.POSSIBLE_FORMATS, EnumSet.allOf(BarcodeFormat.class));
        hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
        hints.put(DecodeHintType.TRY_HARDER, true);

        try {
            Result result = new MultiFormatReader().decode(bitmap, hints);

            //System.out.println(result);


            DriverLicense dl = new DriverLicense(result.getText());
            System.out.println("-----"+dl.toJson());

            return dl.toJson();

        } catch (NotFoundException e) {
            System.out.println("There is no QR code in the image");
            return "There is no QR code in the image";
        }
        catch (Exception e){
            return "Not able to scan or Image is not proper -- please try again";
        }
    }

    public static void main(String[] args) {
//        try {
//            File file = new File("/Users/bhanuprakash/Documents/dl26.jpeg");
//            String decodedText = decodeQRCode(file);
//            if(decodedText == null) {
//                System.out.println("No QR Code found in the image");
//            } else {
//                System.out.println("Decoded text = " + decodedText);
//                DriverLicense dl = new DriverLicense(decodedText);
//                System.out.println("-----"+dl.toJson());
//
//            }
//        } catch (IOException e) {
//            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage());
//        }
    }
}
