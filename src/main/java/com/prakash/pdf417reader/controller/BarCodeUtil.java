package com.prakash.pdf417reader.controller;


import com.prakash.pdf417reader.scanner.QRCodeReader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 *
 * @author Prakash Palnati
 */
@Controller
public class BarCodeUtil {

    @GetMapping("/")
    public String UploadPage(Model model) {
        return "index";
    }

    @PostMapping("/scan")
    public String scanBarCode(Model model, @RequestParam("file") MultipartFile file,
                              RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("msg", "Please select a file to upload");
            return "redirect:status";
        }

        try {
            byte[] bytes = file.getBytes();
            InputStream is = new ByteArrayInputStream(bytes);

            //byte[] decodedString = Base64.getDecoder().decode(bytes);

            model.addAttribute("msg", QRCodeReader.decodeQRCode(is));

            return  "status";

        } catch (IOException e) {
            e.printStackTrace();
            model.addAttribute("msg", "some problem in scanning please raise a ticket");

            return "status";
        }

    }
}
