This Project holds a simple service which scans barcodes on US/Canada licenses and returns a JSON response.

We are using the infamous [zxing](https://github.com/zxing/zxing) library to scan barcoede. Also future plans include 
pre-processing of image to enhance the ROI for Zxing to get better results.